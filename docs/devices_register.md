# Geräte registrieren

Bevor man eine Prüfung auf ein Gerät laden kann, muss man die BeAxi App aus dem App Store
herunterladen. Öffnet man die App, muss man das iPad registrieren. Um das Gerät zu
registrieren, verlangt BeAxi einen QR-Code. Dazu kann man entweder den QR-Code unter dem
Menüpunkt **Dashboard** des Exam Managers scannen, oder im Menü **Geräte** auf iPad-Konfiguration
(1.) klicken und danach im Dropdown-Menü die eigene Institution auswählen (2.; in diesem
Beispiel «Dokumentation»). Es erscheint ein QR-Code, welcher mit dem iPad gescannt werden
kann. Klickt man auf neu laden erscheint das iPad in der Tabelle und ist somit im Exam
Manager registriert (4.). Mit den Dropdown-Menüs **Zuweisen zu** und **Entfernen von** (5.) können
die Geräte unterschiedlichen Institutionen zugeordnet werden.


Der zweite Weg führt über die Prüfung selbst. Wählt man die Prüfung (hier «Einführung in
die Psychologie») im Menüpunkt **Prüfungen** aus, werden im Reiter Fragen (1.) bereits die
Fragen angezeigt, welche derselben Stufe zugeteilt wurden wie die Prüfung (2.). Per Klick
auf den Pfeil (3.) können die Fragen direkt der Prüfung zugeteilt werden.

![](images/devices_register_01.png)