# Anleitung BeAxi ExamManager

Diese Seite gibt Ihnen einen Überblick über die wichtigsten Funktionen des BeAxi Exam Managers.
Folgende Fragen sollen dabei geklärt werden:

1. [Wie erstellt man Prüfungsfragen?](questions_create.md)
2. [Wie erstellt man eine Prüfung?](assessments_create.md)
3. [Wie fügt man Fragen einer Prüfung zu?](questions_assign.md)
4. [Wie teilt man Prüfungskandidaten einer Prüfung zu?](assessments_candidates.md)
5. [Wie registriert man die Geräte?](devices_register.md)
6. [Wie stellt man die Prüfung für die Durchführung bereit?](assessments_prepare.md)
7. [Wie importiert und evaluiert man Prüfungsdaten?](assessments_import.md)
8. [Wie stellt man Feedback/Einsichten vergangener Prüfungen bereit?](assessments_feedback.md)