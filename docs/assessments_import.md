# Prüfungsdaten importieren und evaluieren

Ist die Prüfung abgeschlossen, können die Prüfungsdaten im Menüpunkt **Prüfungen** in den
Exam Manager importiert werden. Dafür wählt man zuerst die Prüfung aus, und klickt im
Reiter auf **Import** (1.) und dann auf **Prüfungsdaten importieren** (2.). Wichtig:
Nachdem die Prüfungsdaten importiert wurden, können solange keine neuen Prüfungsdaten
importiert werden, bis die alten Prüfungsdaten gelöscht wurden (3.).

![](images/assessments_import_01.png)

Unter dem Reiter **Korrektur** (1.) können die Antworten der Kandidaten überprüft werden. Die
Kandidaten werden dabei links (2.) angezeigt und die beantworteten Fragen des Kandidaten
oben (3.). Diese Ansicht kann aber per Klick auf **Kandidaten & Frage-Ansicht** (4.) umgekehrt
werden, so dass die Fragen auf der linken Seite und die Kandidaten oben angezeigt werden
(z.B. sinnvoll wenn eine Frage über alle Kandidaten hinweg korrigiert werden muss).
Weiterhin können die Auto-korrigierten Fragen (5.) ausgeblendet werden, so dass
beispielsweise nur noch Fragetypen mit offenen Antwortfeldern angezeigt werden. Die
automatisch korrigierbaren Fragen (z.B. Multiple Choice) können hier per Klick auf
Ergebnisse berechnen (6.) direkt ausgewertet werden. Offene Fragen können direkt im System
korrigiert werden, sie können aber auch als PDF exportiert werden (7.). Im PDF können die
Antworten dann bewertet werden und per Import (8.) direkt in das System geladen werden.

![](images/assessments_import_02.png)

Unter dem Reiter **Ergebnisse** können die Prüfungsdaten eingesehen und als **Excel-** (1.) oder
**Zip-Datei** (2.) exportiert werden.

![](images/assessments_import_03.png)