# Kandidaten zu Prüfung hinzufügen

Nachdem unter dem Menüpunkt **Prüfungen** die Prüfung ausgewählt wurde, (hier «Einführung in
die Psychologie») kann unter dem Reiter **Kandidaten** (1.) die Kandidatenliste der
Prüfungsteilnehmenden aufgerufen werden. Die Kandidatenliste kann über den Button Import
(2.) per Excel- oder CSV-Datei in das System importiert werden. Damit der **Import**
funktioniert, muss zwingend eine Kopfzeile vorhanden sein, in welcher die Felder
bezeichnet sind. Die Felder **Kandidaten-ID** und **E-Mail** sind dabei zwingend erforderlich. Als
Kandidaten-ID kann beispielsweise die Matrikelnummer der Prüfungsteilnehmenden angegeben
werden. Da die Kandidaten ihre Kandidaten-ID zur Beginn der Prüfung angeben müssen, kann
nachträglich nachvollzogen werden, wer an der Prüfung teilgenommen hat. Die
Kandidatenliste kann nach dem Import als PDF exportiert werden (3.). Dieses PDF wird
direkt mit einem individuellen QR-Code für jeden Kandidaten erstellt. Statt der manuellen
Eingabe von Information zu Beginn der Prüfung, könnte den Kandidaten auch ihre
individuellen QR-Codes zum Scannen vorgelegt werden (dies entspricht Seite 9, Kapitel 2.
Wie erstellt man eine Prüfung?; Angaben zu den iPad Einstellungen).

![](images/assessments_candidates_01.png)
