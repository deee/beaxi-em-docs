# Fragen zu Prüfung hinzufügen

Hat man sowohl die Fragen als auch die Prüfung erstellt, kann man die Fragen einer Prüfung
zuordnen. Wichtig dabei ist, dass den Fragen dieselben Stufen (z.B. 1. Studienjahr, 2.
Semester) zugeordnet wurden wie der Prüfung. Zwei Wege sind möglich: Im Menüpunkt **Fragen**
wählt man diejenigen Fragen aus, welche einer Prüfung zugeordnet werden sollen (1.) und
klickt jeweils auf den grünen Button Einer Prüfung zuweisen (2.). Eine Frage kann erst
dann einer Prüfung zugeordnet werden, wenn sie den Status Aktiv besitzt, also den
Reviewprozess absolviert hat, oder vom Administrator direkt in diesen Status versetzt
wurde.

![](images/questions_assign_01.png)

Der zweite Weg führt über die Prüfung selbst. Wählt man die Prüfung (hier «Einführung in
die Psychologie») im Menüpunkt **Prüfungen** aus, werden im Reiter Fragen (1.) bereits die
Fragen angezeigt, welche derselben Stufe zugeteilt wurden wie die Prüfung (2.). Per Klick
auf den Pfeil (3.) können die Fragen direkt der Prüfung zugeteilt werden.

![](images/questions_assign_02.png)
