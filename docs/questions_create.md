# Fragen erstellen

Um eine neue Frage zu erstellen, klickt man im Menü auf **Fragen** (1.) und anschliessend
auf **+ Neu** (2.).

![Frage erstellen](images/questions_create_01.png)

Der **Frage-Assistent** öffnet sich. Unter dem Reiter **Fragetyp** (1.) kann zwischen einer
Vielzahl an unterschiedlichen Fragetypen gewählt werden (2.). Die unterschiedlichen Fragetypen
sind im Dokument **Anleitung Prüfungsapplikation** unter dem Menüpunkt **Dashboard** genauer beschrieben.

![Frage-Typ wählen](images/questions_create_02.png)

Wurde ein Fragetyp ausgewählt, wird man automatisch auf die Seite **Frage & Antworten** weitergeleitet.
Auf dieser Seite kann definiert werden, wie viele Punkte eine Frage bei korrekter Antwort
erhalten soll (1; z.B. 1 Punkt bei komplett richtig gelöster Aufgabe, ansonsten 0 Punkte).
Weiterhin kann man auf dieser Seite den Fragetext eingeben (2.), ein Bild oder ein anderes
Medium einfügen (3.) und die Antwortmöglichkeiten erstellen (4.). Für jede Antwortmöglichkeit
muss ausserdem definiert werden, ob sie korrekt oder falsch ist (5.).

![Frage eingeben](images/questions_create_03.png)

---
**HINWEIS**

Folgende Medien können in das System integriert werden: 

 - Bilder: "png", "jpg", "jpeg", "jpe", "jfif", "gif", "tif", "tiff", "img", "bmp"
 - Audio: "mp3"
 - Video: "WebM", "ogv", "mp4«

---

Beachten Sie, dass Sie unter dem Reiter **Zusätzliche Informationen**, abhängig davon ob bei
Ihnen der Reviewprozess aktiviert ist, unterschiedlich viele Einstellungen machen können.
Der folgende Abschnitt bezieht sich auf die Möglichkeiten **ohne Reviewprozess** (die Angaben
beziehen sich auf die untere Abbildung): Sichtbar ist der Autor der Frage (1.). Der Name
der Person, welche die Frage erstellt, sollte im Feld bereits eingetragen sein. Unter
**Klassifizierung** (2.) kann man die Frage einem Themenblock zuordnen (z.B. Thema Biologie).
Weiterhin kann ein Kommentar zur Frage angegeben werden (3.). Zum Schluss muss die Frage
gespeichert werden (a.).

Dieser Abschnitt bezieht sich auf die Möglichkeiten, im Falle eines **aktivierten Reviewprozesses**:
Unter **Autor & Reviewer** können Sie zusätzlich zum Autor mögliche Reviewer angeben (1.).
Wird die Frage gespeichert (a.), wird sie in den Im Kreativ-Status versetzt (sichtbar in
der Frageliste im Menü unter **Fragen**). Das bedeutet, dass die Frage noch in Bearbeitung ist.
Wurde zum Autor ein zusätzlicher Reviewer bestimmt, sollte die Frage in den Review Status
(b.) gesetzt werden. Die Frage kann dann erst wieder bearbeitet werden, wenn der Reviewprozess
abgeschlossen ist. Falls kein zusätzlicher Reviewer bestimmt wurde, kann der Administrator
(meist der Frageersteller) die Frage direkt in den Aktiv Status (c.) versetzen. Die Frage
kann dann direkt einer Prüfung zugeordnet werden*. Unter **Klassifizierung** (2.) kann der
Themenblock der Frage angegeben werden (z.B. Thema Biologie). Weiterhin muss die Stufe
angeben werden, wobei 1.2 beispielsweise bedeutet, dass die Prüfung im 1. Studienjahr, im
2. Semester stattfindet. Damit eine Frage einer Prüfung zugeordnet werden kann, muss die
Prüfung derselben Stufe entsprechen wie die Frage (mehr dazu auf Seite 7 im Kapitel 2. Wie
erstellt man eine Prüfung). Zum Schluss kann ein Kommentar zur Frage abgeben werden (3.).

![Frage - zusätzliche Attribute](images/questions_create_04.png)

Die meisten Fragetypen sind ähnlich aufgebaut. Ausnahmen bilden dabei die Fragetypen **Im
Bild zeigen** und **Bild beschriften**. Die Eingaben unter dem Reiter **Frage & Antworten** unterscheiden
sich zwischen den beiden Fragetypen nicht. Unter **Frage** lässt sich, wie bei den anderen
Fragetypen, der Fragetext einfügen (1.) Wichtig für diese beiden Fragetypen ist das Bild,
welches gleich darunter eingefügt werden kann (2.). Per Klick auf Felder ausblenden (3.)
kann man die Antwortmöglichkeiten im Bild einfügen. 

![Bild zeigen beschriften](images/questions_create_05.png)

Beim Fragtypen **Im Bild zeigen** könnte die Frage beispielsweise lauten: «Finde die Nase der
Katze». Prüfungskandidaten müssten also die Nase der Katze anklicken, um die Frage richtig
zu beantworten. Als Antwort muss also der Bereich um die Nase der Katze definiert werden.
Beim Fragetyp **Bild beschriften** könnte die Frage lauten: «Wie heissen die Körperteile der
Katze?». Auf dem Bild können verschiedene Punkte gesetzt werden und mit den richtigen
Antworten versehen werden:

Im Bild zeigen             |  Bild beschriften
:-------------------------:|:-------------------------:
![Im Bild zeigen](images/questions_create_06_1.png)  |  ![Bild beschriften](images/questions_create_06_2.png)