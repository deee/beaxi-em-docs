# Prüfungs-Feedback/Einsicht erstellen
# 
Die Prüfungskandidaten können ihre Prüfungsergebnisse direkt über das iPad einsehen. Dafür
wählt man im Menüpunkt **Geräte** unter **Befehl** (1.) im Dropdownmenü **Prüfung auf Gerät einsehen**
(2.) aus. Wenn man nun die Prüfung und den Kandidaten auswählt, kann die Prüfungseinsicht
direkt auf das iPad geladen werden:

![](images/assessments_feedback_01.png)

Im Menüpunkt **Prüfungen** unter dem Reiter **Kandidaten-Feedback** (1.) können die Kandidaten
direkt per Mail über ihre Ergebnisse informiert werden. Dazu wählt man auf der linken
Seite die Kandidaten (2.), welche man benachrichtigen möchte. Die individuellen Ergebnisse
werden automatisch für jeden Kandidaten zusammengestellt.

![](images/assessments_feedback_02.png)

Unter dem Reiter **Prüfer-Feedback** (1.) kann dem Prüfungsdurchführenden (z.B. Dozent)
schliesslich per Mail eine Zusammenfassung der Ergebnisse zugesendet werden. Dabei können
wiederum die Prüfer (festgelegt bei der Prüfungserstellung im Menüpunkt **Prüfungen**) auf der
linken Seite ausgewählt werden (2.).

![](images/assessments_feedback_03.png)