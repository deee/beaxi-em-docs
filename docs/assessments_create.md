# Prüfung erstellen

Sind die Prüfungsfragen erstellt, folgt als nächster Schritt die Erstellung der Prüfung.
Um eine **Prüfung** zu erstellen, klickt man im Menü auf Prüfungen (1.) und anschliessend
auf **+ Neu** (2.).

![](images/assessments_create_01.png)

Es öffnet sich der **Prüfungsverwaltungs-Assistent**. Im Reiter **Prüfung verwalten**
können die **allgemeinen Eigenschaften** (1.), **Einstelllungen zur Frageneigabe** (2.)
gemacht und **prüfungsspezifische Eigenschaften** (3.) bestimmt werden.

![](images/assessments_create_02.png)

In den **allgemeinen Eigenschaften** (1.) muss das Prüfungsdatum (a.), der Prüfungsname
(b.; z.B. «Einführung in die Psychologie») sowie die Stufe (c.; z.B. 1.2. = 1. Jahr, 2.
Semester) angegeben werden. Sind die Fragen derselben Stufe zugeteilt, können sie der
Prüfung zugeordnet werden. Der Veranstaltungsort (d.; z.B. Basel) kann fakultativ
angegeben werden. Falls es sich um eine Wiederholungsprüfung handelt, kann zusätzlich die
originale Prüfung angegeben werden (e.). Unter **Frageneingabe** (2.) kann angegeben
werden, von wann (f.) bis wann (g.) die Fragen in das System eingepflegt werden können.
Unter **zu korrigieren bis** (h.) kann angegeben werden, bis wann die Prüfung korrigiert
werden soll. Ist dieses Datum überschritten, können keine Änderungen an der Punktevergabe
mehr vorgenommen werden. Dieses Datum entspricht dem Zeitpunkt, an dem die
Prüfungsresultate dem Prüfungssekretariat übergeben werden müssen. Unter
**Prüfungsspezifische Eigenschaften** (3.) kann schliesslich die Bestehensgrenze
festgelegt werden (z.B. 60% richtige Antworten notwendig um die Prüfung zu bestehen).

Unter dem Reiter **iPad Einstellungen** können Einstellungen für die Darstellung der
Prüfung auf dem iPad vorgenommen werden.

![](images/assessments_create_03.png)

In den **Allgemeinen Eigenschaften** (1.) kann festgelegt werden, ob vor dem Prüfungsstart
ein Test-Ton ertönen soll (a.). Hat die Prüfung Audio- oder Videomaterial, kann so
sichergestellt werden, dass die Kopfhörer der Prüfungsteilnehmenden funktionieren.

Um sicherzustellen, dass alle Prüfungsteilnehmenden die Prüfung zur gleichen Zeit starten
können, kann ein Start-Code festgelegt werden (b.). Dieser wird erst bekannt gegeben, wenn
alle Prüfungsteilnehmenden bereit sind, die Prüfung zu starten. Weiterhin kann bei der
Anmeldung zur Prüfung eine Kandidaten-Verifikation (c.) verlangt werden (z.B. Unterschrift
der Prüfungsteilnehmenden). Um sich für die Prüfung anzumelden, kann weiterhin die Eingabe
der Kandidaten-ID (z.B. Matrikelnummer) verlangt werden (d.). Diese Kandidaten-IDs können
vorab in den Exam Manager eingepflegt werden (siehe Seite 11 im Kapitel Wie teilt man
Prüfungskandidaten einer Prüfung zu?)  und dienen dazu, nachträglich nachvollziehen zu
können, wer an der Prüfung teilgenommen hat. Weiterhin kann in den **allgemeinen
Eigenschaften** (1.) angegeben werden, ob für die Prüfung eine externe Tastatur benötigt
wird (e.). Schliesslich hat man die Möglichkeit ein Kamera- Icon bereitzustellen, damit
Studierende ggf. ihre Login-Informationen mithilfe eines persönlichen QR-Codes übertragen
können (f.) (mehr dazu im Kapitel 4. Wie teilt man Prüfungskandidaten einer Prüfung zu?
Seite 11).

Unter den **iPad Einstellungen** (2.) können Einstellungen darüber vorgenommen
werden, was in der App während der Prüfung sichtbar ist. So kann z.B. verhindert werden,
dass Notizen gemacht werden können etc.. Die Fragen der Prüfung werden für jeden
Kandidaten grundsätzlich randomisiert. In den **Randomisierungs-Einstellungen** (3.) können
weitere Einstellungen vorgenommen werden:

Um die **Randomisierungs-Einstellungen** freizuschalten, muss man ein Häkchen setzen bei
**Vordefinierte Anzahl Prüfungs-Versionen verwenden** (1.). Unter dem Drop-Down Menü **Anzahl
Prüfungsversionen** (2.) kann festgelegt werden, wie viele Prüfungsversionen vom System
erstellt werden. Wählt man beispielsweise 2 aus, werden zwei unterschiedliche
Prüfungsversionen erstellt, welche randomisiert auf die iPads verteilt werden. Weiterhin
kann man nach Themenblock (beispielsweise bei 3.), nach Fragetyp  oder nach Frage-ID
sortieren. Sortieren nach Themenblock lohnt sich nur, wenn die Fragen der Prüfung in
unterschiedliche Themenbereiche aufgeteilt wurden.

![](images/assessments_create_04.png)