# Prüfung bereitstellen

Um eine Prüfung für die Durchführung bereitzustellen, wählt man wiederum im Menüpunkt
**Prüfungen** die Prüfung aus, und klickt auf den Reiter **Durchführung** (1.).

![](images/assessments_prepare_01.png)

Die Prüfung kann über den Button **Bereitstellen** (2.) für die Durchführung aktiviert
werden. Die Prüfung, welche sich bislang noch _in Bearbeitung_ (3.) befand, ist nun
_Bereit_ (4.) für die Durchführung (siehe nächste Seite). Es erscheint ein für jede
Prüfung ein individueller **QR-Code** (5.), mit welchem man die Prüfung direkt auf die
iPads laden kann. Dazu klickt man auf dem iPad auf das Kamera-Icon und hält die Kamera
über den QR-Code. Die Prüfung wird direkt auf das iPad geladen.

![](images/assessments_prepare_02.png)

Alternativ kann die Prüfung auch folgendermassen auf die Geräte geladen werden: Man wählt
im Menü **Geräte**, zuerst die Geräte aus, auf welche die Prüfung geladen werden soll (1.).
Dann wählt man unter **Befehl** (2.) im Dropdownmenü **Prüfung auf Gerät laden** aus (3.), und
schliesslich die Prüfung, welche man auf das Gerät laden möchte (4.). Per Klick auf **Senden**
(5.) wird die Prüfung auf die ausgewählten iPads geladen und dort direkt im App angezeigt.

![](images/assessments_prepare_03.png)

Die Prüfung kann nun über die iPads durchgeführt werden. Nach der Prüfung kann unter dem
Reiter Kandidaten im Menüpunkt **Prüfungen** überprüft werden, welche Kandidaten an der
Prüfung anwesend waren.
